package com.oreillyauto.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="university")
public class Student implements Serializable{

    private static final long serialVersionUID = 4947711451649476563L;
    

    public Student() {
        
    }
    
    public Student(String txId, String courseId, String firstName, String lastName, String enrollDate) {
        super();
        //this.txId = txId;
        this.courseId = courseId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.enrollDate = enrollDate;
        
    }
    
    @Id
    @Column(name = "tx_id", columnDefinition = "INTEGER")
    private String txId;
    
    @Column(name = "course_id", columnDefinition = "INTEGER")
    private String courseId;
    
    @Column(name = "first_name", columnDefinition = "VARCHAR(128)")
    private String firstName;
    
    @Column(name = "last_name", columnDefinition = "VARCHAR(128)")
    private String lastName;
    
    @Column(name = "enroll_date", columnDefinition = "TIMESTAMP")
    private String enrollDate;


    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEnrollDate() {
        return enrollDate;
    }

    public void setEnrollDate(String enrollDate) {
        this.enrollDate = enrollDate;
    }

    @Override
    public String toString() {
        return "Student [txId=" + txId + ", courseId=" + courseId + ", firstName=" + firstName + ", lastName=" + lastName + ", enrollDate="
                + enrollDate + "]";
    }
}
