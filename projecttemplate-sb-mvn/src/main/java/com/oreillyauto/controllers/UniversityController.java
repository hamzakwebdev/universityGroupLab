package com.oreillyauto.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.oreillyauto.domain.Student;

@Controller
public class UniversityController {
    
    @GetMapping(value = {"/university"})
    public String getUniversity() {
        return "university";
    }

    @GetMapping(value = {"/university/newStudent"})
    public String getNewStudent() {
        return "newStudent";
    }

    @GetMapping(value = {"/university/courseReport"})
    public String getCourseReport() {
        return "courseReport";
    }
    
    @PostMapping(value = {"/university/newStudent"})
    public String postNewStudent(Model model, String courseName, String firstName, String lastName, String enrollmentDate) {
        Student student = new Student();
        if(firstName.length()>0) {
            student.setCourseId(courseName);
            student.setFirstName(firstName);
            student.setLastName(lastName);
            student.setEnrollDate(enrollmentDate);
            System.out.println(student.toString());
            
            model.addAttribute("messageType", "success");
            model.addAttribute("message", "Course #" + courseName + " added for "+ firstName + " " + lastName);
        }
            
           
        
        
        return "newStudent";
    }
}
