package com.oreillyauto.service.impl;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CarpartsRepository;
import com.oreillyauto.domain.carparts.Carpart;
import com.oreillyauto.dto.Email;
import com.oreillyauto.service.CarpartsService;

@Service("carpartsService")
public class UniversityServiceImpl implements UniversityService {
    @Autowired
    CarpartsRepository carpartsRepo;
    
    @Override
    public void saveCarpart(Carpart carpart) {
        carpartsRepo.save(carpart);
    }
    
    @Override
    public Carpart getCarpartByPartNumber(String partNumber) throws Exception {     
        // Old way (fake it 'til you make it)
        //return carpartsRepo.getCarpartByPartNumber(partNumber);
        
        // Querydsl
        return carpartsRepo.getCarpart(partNumber);
        
        // CRUD REPO API
        //return carpartsRepo.findById(partNumber);
        
        // SPRING DATA API
        //return carpartsRepo.findByPartNumber(partNumber);
    }

    @Override
    public List<Carpart> getCarparts() {
        //return carpartsRepo.getCarparts();
        
        // CRUD REPO API
        return (List<Carpart>)carpartsRepo.findAll();
    }

    @Override
    public void deleteCarpartByPartNumber(Carpart carpart) {
        carpartsRepo.delete(carpart);
    }

    @Override
    public void sendEmail(Email email) throws Exception {
        Properties appProperties = new Properties();
        appProperties.load(CarpartServiceImpl.class.getClassLoader().getResourceAsStream("app.properties"));
        final String authEmailAddress = appProperties.getProperty("gmail.username");
        final String authPassword = appProperties.getProperty("gmail.password");
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(authEmailAddress, authPassword);
            }
        });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(email.getEmailAddress()));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(authEmailAddress));
        message.setSubject("Contact Us Submission");
        message.setText(email.getEmailBody());
        Transport.send(message);
    }

}
