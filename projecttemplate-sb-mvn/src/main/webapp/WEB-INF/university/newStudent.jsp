<%@ include file="/WEB-INF/layouts/include.jsp"%>
<div class="row">
	<div class="col-sm-12">
		<form is="orly-form" id="form" method="post" action="<c:url value='/university/newStudent' />">
			<c:if test="${not empty carpart}">
			    <input type="hidden" id="update" name="update" value="true" />
			</c:if>
			<div class="col-sm-4 form-group">
			  <label for="courseName">Course Name</label>
			  <select required name="courseName" value="${carpart.partNumber}" placeholder="Course Name">
			  	<option value="1">Intro To CS</option>
			  	<option value="2">Biology</option>
			  </select>
			  	
			</div>
			
			<div class="col-sm-4 form-group">
			  <label for="firstName">First Name</label>
			  <orly-input required name="firstName" value="${carpart.title}"  placeholder="First Name"></orly-input>
			</div>
			<div class="col-sm-4 form-group">
			  <label for="lasttName">Last Name</label>
			  <orly-input required name="lastName" value="${carpart.title}"  placeholder="Last Name"></orly-input>
			</div>
			<div class="col-sm-4 form-group">
			  <label for="enrollmentDate">Enrollment Date</label>
			  <orly-input required name="enrollmentDate" value="${carpart.partNumber}" type = "date" placeholder="Enrollment Date"></orly-input>
			</div>
			<div class="col-sm-4 form-group">
			  <!-- <orly-btn id="submitBtn" type="submit" spinonclick text="Submit"></orly-btn> -->
			  <button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>
		</div>
	</div>